﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fotogrametri.UcusPlanlama.Core
{
    public enum Birim
    {
        _,
        km,
        hm,
        dm,
        m,
        cm,
        mm,
        micron,
        km_h,
        m_sn,
        derece,
        adet,
        px,
        cm2,
        m2,
        ha,
        km2,
        sn,
        sn_foto,
        yüzde
    }
}
