﻿using System;
using System.ComponentModel;

namespace Fotogrametri.UcusPlanlama.Core
{
    [DefaultPropertyAttribute("Val")]
    public class BirimliDouble
    {
        public double Val { get; set; }
        public Birim Birim { get; set; }
        public BirimliDouble(double val, Birim birim)
        {
            this.Val = val;
            this.Birim = birim;
        }

        #region " Operators "
        // BirimliDouble - BirimliDouble
        public static BirimliDouble operator +(BirimliDouble i1, BirimliDouble i2) { return new BirimliDouble(i1.Val + i2.Val, i1.Birim); }
        public static BirimliDouble operator -(BirimliDouble i1, BirimliDouble i2) { return new BirimliDouble(i1.Val - i2.Val, i1.Birim); }
        public static BirimliDouble operator /(BirimliDouble i1, BirimliDouble i2) { return new BirimliDouble(i1.Val / i2.Val, i1.Birim); }
        public static BirimliDouble operator *(BirimliDouble i1, BirimliDouble i2) { return new BirimliDouble(i1.Val * i2.Val, i1.Birim); }
        public static bool operator ==(BirimliDouble i1, BirimliDouble i2) { return i1.Val == i2.Val && i1.Birim == i2.Birim; }
        public static bool operator !=(BirimliDouble i1, BirimliDouble i2) { return i1.Val != i2.Val || i1.Birim != i2.Birim; }
        // BirimliInt - BirimliDouble
        public static BirimliDouble operator +(BirimliInt i1, BirimliDouble i2) { return new BirimliDouble(i1.Val + i2.Val, i1.Birim); }
        public static BirimliDouble operator -(BirimliInt i1, BirimliDouble i2) { return new BirimliDouble(i1.Val - i2.Val, i1.Birim); }
        public static BirimliDouble operator /(BirimliInt i1, BirimliDouble i2) { return new BirimliDouble(i1.Val / i2.Val, i1.Birim); }
        public static BirimliDouble operator *(BirimliInt i1, BirimliDouble i2) { return new BirimliDouble(i1.Val * i2.Val, i1.Birim); }
        public static bool operator ==(BirimliInt i1, BirimliDouble i2) { return i1.Val == i2.Val && i1.Birim == i2.Birim; }
        public static bool operator !=(BirimliInt i1, BirimliDouble i2) { return i1.Val != i2.Val || i1.Birim != i2.Birim; }
        // BirimliDouble - BirimliInt
        public static BirimliDouble operator +(BirimliDouble i1, BirimliInt i2) { return new BirimliDouble(i1.Val + i2.Val, i1.Birim); }
        public static BirimliDouble operator -(BirimliDouble i1, BirimliInt i2) { return new BirimliDouble(i1.Val - i2.Val, i1.Birim); }
        public static BirimliDouble operator /(BirimliDouble i1, BirimliInt i2) { return new BirimliDouble(i1.Val / i2.Val, i1.Birim); }
        public static BirimliDouble operator *(BirimliDouble i1, BirimliInt i2) { return new BirimliDouble(i1.Val * i2.Val, i1.Birim); }
        public static bool operator ==(BirimliDouble i1, BirimliInt i2) { return i1.Val == i2.Val && i1.Birim == i2.Birim; }
        public static bool operator !=(BirimliDouble i1, BirimliInt i2) { return i1.Val != i2.Val || i1.Birim != i2.Birim; }
        // BirimliDouble - double
        public static BirimliDouble operator +(BirimliDouble i1, double i2) { return new BirimliDouble(i1.Val + i2, i1.Birim); }
        public static BirimliDouble operator -(BirimliDouble i1, double i2) { return new BirimliDouble(i1.Val - i2, i1.Birim); }
        public static BirimliDouble operator /(BirimliDouble i1, double i2) { return new BirimliDouble(i1.Val / i2, i1.Birim); }
        public static BirimliDouble operator *(BirimliDouble i1, double i2) { return new BirimliDouble(i1.Val * i2, i1.Birim); }
        // double - BirimliDouble
        public static BirimliDouble operator +(double i1, BirimliDouble i2) { return new BirimliDouble(i1 + i2.Val, i2.Birim); }
        public static BirimliDouble operator -(double i1, BirimliDouble i2) { return new BirimliDouble(i1 - i2.Val, i2.Birim); }
        public static BirimliDouble operator /(double i1, BirimliDouble i2) { return new BirimliDouble(i1 / i2.Val, i2.Birim); }
        public static BirimliDouble operator *(double i1, BirimliDouble i2) { return new BirimliDouble(i1 * i2.Val, i2.Birim); }
        // BirimliDouble - int
        public static BirimliDouble operator +(BirimliDouble i1, int i2) { return new BirimliDouble(i1.Val + i2, i1.Birim); }
        public static BirimliDouble operator -(BirimliDouble i1, int i2) { return new BirimliDouble(i1.Val - i2, i1.Birim); }
        public static BirimliDouble operator /(BirimliDouble i1, int i2) { return new BirimliDouble(i1.Val / i2, i1.Birim); }
        public static BirimliDouble operator *(BirimliDouble i1, int i2) { return new BirimliDouble(i1.Val * i2, i1.Birim); }
        // int - BirimliDouble
        public static BirimliDouble operator +(int i1, BirimliDouble i2) { return new BirimliDouble(i1 + i2.Val, i2.Birim); }
        public static BirimliDouble operator -(int i1, BirimliDouble i2) { return new BirimliDouble(i1 - i2.Val, i2.Birim); }
        public static BirimliDouble operator /(int i1, BirimliDouble i2) { return new BirimliDouble(i1 / i2.Val, i2.Birim); }
        public static BirimliDouble operator *(int i1, BirimliDouble i2) { return new BirimliDouble(i1 * i2.Val, i2.Birim); }
        #endregion

        public override string ToString()
        {
            if (this.Birim == Birim._)
                return this.Val.ToString();
            else return this.Val.ToString() + " " + this.Birim.ToString().Replace("_","/");
        }

        public BirimliInt ToCeiling(BirimliDouble i)
        {
            return new BirimliInt((int)Math.Ceiling(i.Val), i.Birim);
        }

    }

}
