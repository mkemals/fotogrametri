﻿using System.ComponentModel;

namespace Fotogrametri.UcusPlanlama.Core
{
    [DefaultPropertyAttribute("Val")]
    public class BirimliInt
    {
        public int Val { get; set; }
        public Birim Birim { get; set; }
        public BirimliInt(int val, Birim birim)
        {
            this.Val = val;
            this.Birim = birim;
        }

        #region " Operators "
        // BirimliInt - BirimliInt
        public static BirimliInt operator +(BirimliInt i1, BirimliInt i2) { return new BirimliInt(i1.Val + i2.Val, i1.Birim); }
        public static BirimliInt operator -(BirimliInt i1, BirimliInt i2) { return new BirimliInt(i1.Val - i2.Val, i1.Birim); }
        public static BirimliInt operator /(BirimliInt i1, BirimliInt i2) { return new BirimliInt(i1.Val / i2.Val, i1.Birim); }
        public static BirimliInt operator *(BirimliInt i1, BirimliInt i2) { return new BirimliInt(i1.Val * i2.Val, i1.Birim); }
        public static bool operator ==(BirimliInt i1, BirimliInt i2) { return i1.Val == i2.Val && i1.Birim == i2.Birim; }
        public static bool operator !=(BirimliInt i1, BirimliInt i2) { return i1.Val != i2.Val || i1.Birim != i2.Birim; }
        // BirimliInt - int
        public static BirimliInt operator +(BirimliInt i1, int i2) { return new BirimliInt(i1.Val + i2, i1.Birim); }
        public static BirimliInt operator -(BirimliInt i1, int i2) { return new BirimliInt(i1.Val - i2, i1.Birim); }
        public static BirimliInt operator /(BirimliInt i1, int i2) { return new BirimliInt(i1.Val / i2, i1.Birim); }
        public static BirimliInt operator *(BirimliInt i1, int i2) { return new BirimliInt(i1.Val * i2, i1.Birim); }
        // int - BirimliInt
        public static BirimliInt operator +(int i1, BirimliInt i2) { return new BirimliInt(i1 + i2.Val, i2.Birim); }
        public static BirimliInt operator -(int i1, BirimliInt i2) { return new BirimliInt(i1 - i2.Val, i2.Birim); }
        public static BirimliInt operator /(int i1, BirimliInt i2) { return new BirimliInt(i1 / i2.Val, i2.Birim); }
        public static BirimliInt operator *(int i1, BirimliInt i2) { return new BirimliInt(i1 * i2.Val, i2.Birim); }
        // double - BirimliInt
        public static BirimliDouble operator *(double i1, BirimliInt i2) { return new BirimliDouble(i1 * i2.Val, i2.Birim); }
        public static BirimliDouble operator /(double i1, BirimliInt i2) { return new BirimliDouble(i1 / i2.Val, i2.Birim); }
        // BirimliInt - double
        public static BirimliDouble operator *(BirimliInt i1, double i2) { return new BirimliDouble(i1.Val * i2, i1.Birim); }
        public static BirimliDouble operator /(BirimliInt i1, double i2) { return new BirimliDouble(i1.Val / i2, i1.Birim); }
        #endregion

        public override string ToString()
        {
            if (this.Birim == Birim._)
                return this.Val.ToString();
            else return this.Val.ToString() + " " + this.Birim.ToString().Replace("_", "/");
        }
    }
}
