﻿using System.Collections.Generic;
using System.Drawing;

namespace Fotogrametri.UcusPlanlama
{
    public static class Extension
    {

        //vxdfvdfvdf
        public static Point CenterPoint(this Rectangle rect)
        {
            return new Point(rect.X + rect.Width / 2,
                             rect.Y + rect.Height / 2);
        }

        /// <summary>
        /// 1----2
        /// |    |
        /// 4----3
        /// </summary>
        /// <returns></returns>
        public static List<Point> GetCorners(this Rectangle rect)
        {
            List<Point> res = new List<Point>();
            res.Add(new Point(rect.X, rect.Y)); // 1
            res.Add(new Point(rect.X + rect.Width, rect.Y)); // 2
            res.Add(new Point(rect.X + rect.Width, rect.Y + rect.Height)); // 3
            res.Add(new Point(rect.X, rect.Y + rect.Height)); // 4
            return res;
        }

        public static bool PointInRectangle(this Rectangle rect, Point p)
        {
            return ((rect.X <= p.X && p.X <= rect.X + rect.Width)
                 && (rect.Y <= p.Y && p.Y <= rect.Y + rect.Height));
        }



    }
}
