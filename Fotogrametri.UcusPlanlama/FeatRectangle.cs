﻿using System.Collections.Generic;
using System.Drawing;

namespace Fotogrametri.UcusPlanlama
{
    public class FeatRectangle
    {
        public Rectangle Rectangle { get; set; }
        public bool IsCover { get; set; }
        public Point BasePoint { get; set; }
        public Point CenterPoint { get; set; }
        public List<Point> Corners { get; set; }

        public FeatRectangle(Rectangle rect)
        {
            this.Rectangle = rect;
            this.IsCover = false;
            this.BasePoint = this.Rectangle.CenterPoint();
            this.CenterPoint = this.Rectangle.CenterPoint();
            this.Corners = this.Rectangle.GetCorners();
        }

    }
}
