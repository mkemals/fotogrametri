﻿using System;
using System.ComponentModel;

namespace Fotogrametri.UcusPlanlama.Model
{
    public class Kamera
    {
        public string Model { get; set; }
        public int Pankromatik_PikselX { get; set; }
        public int Pankromatik_PikselY { get; set; }
        public decimal Pankromatik_PikselBoyutu { get; set; }
        public int Pankromatik_OdakMesafesi { get; set; }
        public int Renkli_PikselX { get; set; }
        public int Renkli_PikselY { get; set; }
        public decimal Renkli_PikselBoyutu { get; set; }
        public int Renkli_OdakMesafesi { get; set; }

        [CategoryAttribute("Kamera Model"), DescriptionAttribute("Kameranın fiziksel fotoğraf formatı: Genişlik (mm)")]
        public decimal FizikselFormat_X { get; set; }
        [CategoryAttribute("Kamera Model"), DescriptionAttribute("Kameranın fiziksel fotoğraf formatı: Yükseklik (mm)")]
        public decimal FizikselFormat_Y { get; set; }

        public override string ToString()
        {
            return this.Model;
        }
    }
}
