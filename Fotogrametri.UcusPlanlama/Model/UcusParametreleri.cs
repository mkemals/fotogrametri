﻿using System;
using System.ComponentModel;
using Fotogrametri.UcusPlanlama.Core;

namespace Fotogrametri.UcusPlanlama.Model
{
    [DefaultPropertyAttribute("UcusAcisi")]
    public class UcusParametreleri
    {
        [CategoryAttribute("Uçuş Açıları"), DescriptionAttribute("Uçağın Omega açısı")]
        public BirimliDouble Omega { get; set; } = new BirimliDouble(0, Birim.derece);
        [CategoryAttribute("Uçuş Açıları"), DescriptionAttribute("Uçağın Phi açısı")]
        public BirimliDouble Phi { get; set; } = new BirimliDouble(0, Birim.derece);
        [CategoryAttribute("Uçuş Açıları"), DescriptionAttribute("Uçağın Kappa açısı")]
        public BirimliDouble Kappa { get; set; } = new BirimliDouble(0, Birim.derece);

        [CategoryAttribute("Ölçekler"), DescriptionAttribute("Pafta Ölçeği")]
        public BirimliInt PaftaOlcek { get; set; } = new BirimliInt(1000, Birim._);
        [CategoryAttribute("Ölçekler"), DescriptionAttribute("Fotoğraf ölçeği")]
        public BirimliDouble Mr { get; set; } = new BirimliDouble(0, Birim._);
        [CategoryAttribute("Ölçekler"), DescriptionAttribute("Fotoğraf ölçeği paydası")]
        public BirimliInt mr { get; set; } = new BirimliInt(0, Birim._);

        [CategoryAttribute("Yer Ölçümleri"), DescriptionAttribute("Yer Örnekleme Aralığı-YÖA (Ground Sample Distance-GSD)")]
        public BirimliInt YOA { get; set; } = new BirimliInt(0, Birim.cm);
        [CategoryAttribute("Yer Ölçümleri"), DescriptionAttribute("Fotoğrafın bir kenarının arazideki karşılığı")]
        public BirimliInt S { get; set; } = new BirimliInt(0, Birim.cm);
        [CategoryAttribute("Yer Ölçümleri"), DescriptionAttribute("Fotoğrafın bir kenarının uzunluğu")]
        public BirimliInt s { get; set; } = new BirimliInt(0, Birim.cm);
        [CategoryAttribute("Yer Ölçümleri"), DescriptionAttribute("Bir fotoğrafın kapladığı arazı yüzölçümü")]
        public BirimliInt Fr { get; set; } = new BirimliInt(0, Birim.adet);

        [CategoryAttribute("Sayılar"), DescriptionAttribute("Bir kolondaki model sayısı")]
        public BirimliInt nm { get; set; } = new BirimliInt(0, Birim.adet);
        [CategoryAttribute("Sayılar"), DescriptionAttribute("Bloktaki kolon sayısı")]
        public BirimliInt nk { get; set; } = new BirimliInt(0, Birim.adet);
        [CategoryAttribute("Sayılar"), DescriptionAttribute("Bir kolondaki fotoğraf sayısı")]
        public BirimliInt nr { get; set; } = new BirimliInt(0, Birim.adet);
        [CategoryAttribute("Sayılar"), DescriptionAttribute("Bloktaki model sayısı")]
        public BirimliInt Nm { get; set; } = new BirimliInt(0, Birim.adet);
        [CategoryAttribute("Sayılar"), DescriptionAttribute("Bloktaki fotoğraf sayısı")]
        public BirimliInt Nr { get; set; } = new BirimliInt(0, Birim.adet);

        [CategoryAttribute("Uçuş"), DescriptionAttribute("Uçağın uçuş yönü")]
        public string UcusYonu { get; set; }
        [CategoryAttribute("Uçuş"), DescriptionAttribute("Uçağın uçuş hızı (m/sn)")]
        public BirimliDouble V { get; set; } = new BirimliDouble(55, Birim.m_sn);
        [CategoryAttribute("Uçuş"), DescriptionAttribute("Uçağın proje sahasından uçuş yüksekliği: h")]
        public BirimliInt h { get; set; } = new BirimliInt(50, Birim.m);

        [CategoryAttribute("Bindirme"), DescriptionAttribute("Boyuna bindirme oranı")]
        public BirimliDouble p { get; set; } = new BirimliDouble(60, Birim.yüzde);
        [CategoryAttribute("Bindirme"), DescriptionAttribute("Enine bindirme oranı")]
        public BirimliDouble q { get; set; } = new BirimliDouble(20, Birim.yüzde);

        [CategoryAttribute("Baz Mesafeleri"), DescriptionAttribute("Fotoğraf ölçeğinde baz")]
        public BirimliDouble b { get; set; } = new BirimliDouble(0, Birim.m);
        [CategoryAttribute("Baz Mesafeleri"), DescriptionAttribute("Fotoğraf ölçeğindeki baz'ın arazideki karşılığı")]
        public BirimliDouble B { get; set; } = new BirimliDouble(0, Birim.m);
        [CategoryAttribute("Baz Mesafeleri"), DescriptionAttribute("Kolonlar arası uzunluk")]
        public BirimliDouble a { get; set; } = new BirimliDouble(0, Birim.m);
        [CategoryAttribute("Baz Mesafeleri"), DescriptionAttribute("Kolonlar arası uzunluğun arazideki karşılığı")]
        public BirimliDouble A { get; set; } = new BirimliDouble(0, Birim.m);

        [CategoryAttribute("Kamera Özellikleri"), DescriptionAttribute("Çekim yapılan kamera modeli")]
        public Kamera Kamera { get; set; }
        [CategoryAttribute("Kamera Özellikleri"), DescriptionAttribute("Kamera odak (asal) uzaklığı (mm)")]
        public BirimliInt c { get; set; } = new BirimliInt(100, Birim.mm);
        [CategoryAttribute("Kamera Özellikleri"), DescriptionAttribute("Kamera çekim faz hızı: dt (saniye/foto)")]
        public BirimliDouble dt { get; set; } = new BirimliDouble(0.02, Birim.sn_foto);
        [CategoryAttribute("Kamera Özellikleri"), DescriptionAttribute("Kameranın Boyuna Piksel Sayısı")]
        public BirimliInt PikselSayilariX { get; set; } = new BirimliInt(0, Birim.px);
        [CategoryAttribute("Kamera Özellikleri"), DescriptionAttribute("Kameranın Enine Piksel Sayısı")]
        public BirimliInt PikselSayilariY { get; set; } = new BirimliInt(0, Birim.px);
        [CategoryAttribute("Kamera Özellikleri"), DescriptionAttribute("Kameranın Piksel Büyüklüğü (mikron)")]
        public BirimliDouble Ps { get; set; } = new BirimliDouble(0, Birim.micron);

        [CategoryAttribute("Çalışma Alanı"), DescriptionAttribute("Net model alanı")]
        public BirimliDouble Fm { get; set; } = new BirimliDouble(0, Birim.m2);
        [CategoryAttribute("Çalışma Alanı"), DescriptionAttribute("Proje alanı")]
        public BirimliDouble F { get; set; } = new BirimliDouble(0, Birim.m2);
        [CategoryAttribute("Çalışma Alanı"), DescriptionAttribute("la: Çalışma alanı kolon uzunluğu")]
        public BirimliDouble la { get; set; } = new BirimliDouble(0, Birim.m);
        [CategoryAttribute("Çalışma Alanı"), DescriptionAttribute("lb: Çalışma alanı blok genişliği")]
        public BirimliDouble lb { get; set; } = new BirimliDouble(0, Birim.m);

        [CategoryAttribute("Görüntü Yürümesi"), DescriptionAttribute("İki fotoğraf çekimi arasındaki süre t(sn)")]
        public BirimliDouble Delta_t { get; set; } = new BirimliDouble(0, Birim.sn);
        [CategoryAttribute("Görüntü Yürümesi"), DescriptionAttribute("Görüntü yürümesi (mikron)")]
        public BirimliDouble ds { get; set; } = new BirimliDouble(0, Birim.micron);

        public UcusParametreleri()
        {
        }

        public void Hesapla()
        {
            mr = h * 1000 / c;
            Mr = 1.0 / mr;
            S = mr * s;
            Fr = S * S;
            b = s / 100.0 * (1 - p / 100.0);
            B = mr * b;
            a = s / 100.0 * (1 - q / 100.0);
            A = mr * a;
            //YOA = new BirimliInt((int)Math.Ceiling(mr * Ps / 10000.0), Birim.cm);
            //nm =  (int)Math.Ceiling(lb / B);
            //nk = (int)Math.Ceiling(la / A);
            //nr = (int)Math.Ceiling(lb / B) + 1;
            Nm = nk * nm;
            Nr = nk * nr;
            Fm = A * B;
            F = la * lb;
            Delta_t.Val = Math.Round((B / V).Val, 2);
            ds = V * dt / mr;
        }

    }
}
