﻿using System.Collections.Generic;
using Fotogrametri.UcusPlanlama.Model;

namespace Fotogrametri.UcusPlanlama.Repository
{
    public class KameraModelleri
    {
        public IList<Kamera> Liste { get; set; }

        public KameraModelleri()
        {
            this.Liste = new List<Kamera>();
            this.Liste.Add(new Kamera
            {
                Model = "UltraCam X",
                Pankromatik_PikselX = 14430,
                Pankromatik_PikselY = 9420,
                Pankromatik_PikselBoyutu = 7.2m,
                Pankromatik_OdakMesafesi = 100,
                Renkli_PikselX = 4810,
                Renkli_PikselY = 3190,
                Renkli_PikselBoyutu = 7.2m,
                Renkli_OdakMesafesi = 33,
                FizikselFormat_X = 104.0m,
                FizikselFormat_Y = 68.4m,
            });
            this.Liste.Add(new Kamera
            {
                Model = "Z/I DMC",
                Pankromatik_PikselX = 13824,
                Pankromatik_PikselY = 7680,
                Pankromatik_PikselBoyutu = 14.0m,
                Pankromatik_OdakMesafesi = 120,
                Renkli_PikselX = 4810,
                Renkli_PikselY = 3190,
                Renkli_PikselBoyutu = 14.0m,
                Renkli_OdakMesafesi = 33,
                FizikselFormat_X = 104.0m,
                FizikselFormat_Y = 68.4m,
            });
        }
    }
}
