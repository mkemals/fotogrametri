﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Fotogrametri.UcusPlanlama
{
    public static class Utilities
    {

        public static List<FeatRectangle> CoverRectIntoBound(Rectangle bound, int widthCoverRect, int heightCoverRect, byte wMountingPercent = 60, byte hMountingPercent = 30)
        {
            List<FeatRectangle> res = new List<FeatRectangle>();
            int wMargin = widthCoverRect * wMountingPercent / 100; // enine taşma
            int hMargin = heightCoverRect * hMountingPercent / 100; // boyuna taşma
            int wCount = (int)Math.Ceiling((decimal)(bound.Width + 2 * wMargin) / widthCoverRect);
            int hCount = (int)Math.Ceiling((decimal)(bound.Height + 2 * hMargin) / heightCoverRect);
            for (int i = 0; i < wCount; i++)
            {
                for (int j = 0; j < hCount; j++)
                {
                    res.Add(new FeatRectangle(new Rectangle(bound.X - wMargin + i * widthCoverRect,
                        bound.Y - hMargin + j * heightCoverRect,
                        widthCoverRect,
                        heightCoverRect)));
                }
            }
            return res;
        }

        public static bool RectanglePolygonCollision(List<Point> points, Rectangle rect)
        {
            List<Point> corners = rect.GetCorners();
            foreach (Point p in corners)
            {
                if (PointInPolygon(points, p))
                    return true;
            }
            foreach (Point p in points)
            {
                if (rect.PointInRectangle(p))
                    return true;
            }
            return false;
        }
        //public static bool PointInPolygon(List<Point> points, Point p)
        //{
        //    int countOfIntersection = 0;
        //    int countPolyPoint = points.Count;
        //    int x0 = points[countPolyPoint - 1].X - p.X;
        //    int y0 = points[countPolyPoint - 1].Y - p.Y;
        //    for (int i = countPolyPoint; i > 0; i--)
        //    {
        //        int x1 = points[i - 1].X - p.X;
        //        int y1 = points[i - 1].Y - p.Y;
        //        if ((y0 > 0 && y1 <= 0) && (x1 * y0 > y1 * x0)) countOfIntersection++;
        //        if ((y1 > 0 && y0 <= 0) && (x0 * y1 > y0 * x1)) countOfIntersection++;
        //        x0 = x1;
        //        y0 = y1;
        //    }
        //    return countOfIntersection % 2 == 1;
        //}
        public static bool PointInPolygon(List<Point> points, Point p)
        {
            double minX = points[0].X;
            double maxX = points[0].X;
            double minY = points[0].Y;
            double maxY = points[0].Y;
            for (int i = 1; i < points.Count; i++)
            {
                Point q = points[i];
                minX = Math.Min(q.X, minX);
                maxX = Math.Max(q.X, maxX);
                minY = Math.Min(q.Y, minY);
                maxY = Math.Max(q.Y, maxY);
            }
            if (p.X < minX || p.X > maxX || p.Y < minY || p.Y > maxY)
            {
                return false;
            }
            // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
            bool inside = false;
            for (int i = 0, j = points.Count - 1; i < points.Count; j = i++)
            {
                if ((points[i].Y > p.Y) != (points[j].Y > p.Y) &&
                     p.X < (points[j].X - points[i].X) * (p.Y - points[i].Y) / (points[j].Y - points[i].Y) + points[i].X)
                {
                    inside = !inside;
                }
            }
            return inside;
        }

        public static float PolygonArea(List<Point> points)
        {
            if (points.Count < 3) return -1;
            points.Add(points[0]);
            return Math.Abs(points.Take(points.Count - 1)
                                  .Select((p, i) => (points[i + 1].X - p.X) * (points[i + 1].Y + p.Y))
                                  .Sum() / 2);
        }

    }
}
