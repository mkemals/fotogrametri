﻿namespace Fotogrametri.UcusPlanlama
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbDraw = new System.Windows.Forms.PictureBox();
            this.txtResults = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFlyPlan = new System.Windows.Forms.Button();
            this.llResetPoligon = new System.Windows.Forms.LinkLabel();
            this.trackBar = new System.Windows.Forms.TrackBar();
            this.txtPikselSayisiX = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cmbKamera = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtKGKenari = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDBKenari = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtYukseklik = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtKappa = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtFi = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtOmega = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPikselBuyuklugu = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtFazHizi = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAsalUzaklik = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUcaginHizi = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtQ = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtP = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPikselSayisiY = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbUcusYonu = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbPaftaOlcek = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSonucEkrani = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtFotografFormati = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.pbDraw)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbDraw
            // 
            this.pbDraw.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbDraw.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbDraw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbDraw.Location = new System.Drawing.Point(0, 160);
            this.pbDraw.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbDraw.Name = "pbDraw";
            this.pbDraw.Size = new System.Drawing.Size(966, 384);
            this.pbDraw.TabIndex = 0;
            this.pbDraw.TabStop = false;
            this.pbDraw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbDraw_MouseDown);
            // 
            // txtResults
            // 
            this.txtResults.Location = new System.Drawing.Point(24, 167);
            this.txtResults.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtResults.Multiline = true;
            this.txtResults.Name = "txtResults";
            this.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResults.Size = new System.Drawing.Size(229, 310);
            this.txtResults.TabIndex = 1;
            this.txtResults.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.propertyGrid1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(286, 544);
            this.panel1.TabIndex = 2;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.CommandsVisibleIfAvailable = false;
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.Location = new System.Drawing.Point(5, 42);
            this.propertyGrid1.Margin = new System.Windows.Forms.Padding(4);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            this.propertyGrid1.Size = new System.Drawing.Size(276, 497);
            this.propertyGrid1.TabIndex = 6;
            this.propertyGrid1.ToolbarVisible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnFlyPlan);
            this.panel2.Controls.Add(this.llResetPoligon);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(5, 5);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(276, 37);
            this.panel2.TabIndex = 2;
            // 
            // btnFlyPlan
            // 
            this.btnFlyPlan.AutoSize = true;
            this.btnFlyPlan.Location = new System.Drawing.Point(3, 2);
            this.btnFlyPlan.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFlyPlan.Name = "btnFlyPlan";
            this.btnFlyPlan.Size = new System.Drawing.Size(96, 33);
            this.btnFlyPlan.TabIndex = 0;
            this.btnFlyPlan.Text = "Uçuş Planla";
            this.btnFlyPlan.UseVisualStyleBackColor = true;
            this.btnFlyPlan.Click += new System.EventHandler(this.btnFlyPlan_Click);
            // 
            // llResetPoligon
            // 
            this.llResetPoligon.AutoSize = true;
            this.llResetPoligon.Location = new System.Drawing.Point(103, 12);
            this.llResetPoligon.Name = "llResetPoligon";
            this.llResetPoligon.Size = new System.Drawing.Size(92, 17);
            this.llResetPoligon.TabIndex = 1;
            this.llResetPoligon.TabStop = true;
            this.llResetPoligon.Text = "Planı Temizle";
            this.llResetPoligon.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llResetPoligon_LinkClicked);
            // 
            // trackBar
            // 
            this.trackBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.trackBar.Location = new System.Drawing.Point(5, 549);
            this.trackBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trackBar.Maximum = 180;
            this.trackBar.Name = "trackBar";
            this.trackBar.Size = new System.Drawing.Size(1262, 56);
            this.trackBar.TabIndex = 3;
            this.trackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            // 
            // txtPikselSayisiX
            // 
            this.txtPikselSayisiX.Location = new System.Drawing.Point(661, 42);
            this.txtPikselSayisiX.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPikselSayisiX.Name = "txtPikselSayisiX";
            this.txtPikselSayisiX.Size = new System.Drawing.Size(56, 22);
            this.txtPikselSayisiX.TabIndex = 3;
            this.txtPikselSayisiX.Text = "14430";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.txtFotografFormati);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.cmbKamera);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.txtKGKenari);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.txtDBKenari);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.txtYukseklik);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.txtKappa);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.txtFi);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.txtOmega);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.txtPikselBuyuklugu);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.txtFazHizi);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.txtAsalUzaklik);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.txtUcaginHizi);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.txtQ);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.txtP);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.txtPikselSayisiY);
            this.panel3.Controls.Add(this.txtPikselSayisiX);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.cmbUcusYonu);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.cmbPaftaOlcek);
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.panel3.Size = new System.Drawing.Size(966, 160);
            this.panel3.TabIndex = 4;
            // 
            // cmbKamera
            // 
            this.cmbKamera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKamera.FormattingEnabled = true;
            this.cmbKamera.Location = new System.Drawing.Point(660, 9);
            this.cmbKamera.Margin = new System.Windows.Forms.Padding(4);
            this.cmbKamera.Name = "cmbKamera";
            this.cmbKamera.Size = new System.Drawing.Size(120, 24);
            this.cmbKamera.TabIndex = 41;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(600, 14);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 17);
            this.label24.TabIndex = 40;
            this.label24.Text = "Kamera: ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(917, 129);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 17);
            this.label20.TabIndex = 39;
            this.label20.Text = "m";
            // 
            // txtKGKenari
            // 
            this.txtKGKenari.Location = new System.Drawing.Point(857, 126);
            this.txtKGKenari.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtKGKenari.Name = "txtKGKenari";
            this.txtKGKenari.Size = new System.Drawing.Size(56, 22);
            this.txtKGKenari.TabIndex = 37;
            this.txtKGKenari.Text = "694";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(777, 129);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(82, 17);
            this.label21.TabIndex = 38;
            this.label21.Text = "K-G Kenarı:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(917, 102);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(19, 17);
            this.label22.TabIndex = 36;
            this.label22.Text = "m";
            // 
            // txtDBKenari
            // 
            this.txtDBKenari.Location = new System.Drawing.Point(857, 98);
            this.txtDBKenari.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDBKenari.Name = "txtDBKenari";
            this.txtDBKenari.Size = new System.Drawing.Size(56, 22);
            this.txtDBKenari.TabIndex = 34;
            this.txtDBKenari.Text = "546";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(777, 102);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(81, 17);
            this.label23.TabIndex = 35;
            this.label23.Text = "D-B Kenarı:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(723, 129);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 17);
            this.label18.TabIndex = 33;
            this.label18.Text = "m";
            // 
            // txtYukseklik
            // 
            this.txtYukseklik.Location = new System.Drawing.Point(663, 126);
            this.txtYukseklik.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtYukseklik.Name = "txtYukseklik";
            this.txtYukseklik.Size = new System.Drawing.Size(56, 22);
            this.txtYukseklik.TabIndex = 31;
            this.txtYukseklik.Text = "50";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(587, 129);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 17);
            this.label19.TabIndex = 32;
            this.label19.Text = "Yükseklik: ";
            // 
            // txtKappa
            // 
            this.txtKappa.Location = new System.Drawing.Point(248, 73);
            this.txtKappa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtKappa.Name = "txtKappa";
            this.txtKappa.ReadOnly = true;
            this.txtKappa.Size = new System.Drawing.Size(20, 22);
            this.txtKappa.TabIndex = 29;
            this.txtKappa.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(219, 76);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 17);
            this.label17.TabIndex = 30;
            this.label17.Text = "κ:";
            // 
            // txtFi
            // 
            this.txtFi.Location = new System.Drawing.Point(248, 43);
            this.txtFi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFi.Name = "txtFi";
            this.txtFi.ReadOnly = true;
            this.txtFi.Size = new System.Drawing.Size(20, 22);
            this.txtFi.TabIndex = 29;
            this.txtFi.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(219, 47);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 17);
            this.label15.TabIndex = 30;
            this.label15.Text = "φ:";
            // 
            // txtOmega
            // 
            this.txtOmega.Location = new System.Drawing.Point(248, 12);
            this.txtOmega.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOmega.Name = "txtOmega";
            this.txtOmega.ReadOnly = true;
            this.txtOmega.Size = new System.Drawing.Size(20, 22);
            this.txtOmega.TabIndex = 29;
            this.txtOmega.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(219, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 17);
            this.label16.TabIndex = 30;
            this.label16.Text = "ω:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(724, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 17);
            this.label10.TabIndex = 24;
            this.label10.Text = "μ";
            // 
            // txtPikselBuyuklugu
            // 
            this.txtPikselBuyuklugu.Location = new System.Drawing.Point(663, 70);
            this.txtPikselBuyuklugu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPikselBuyuklugu.Name = "txtPikselBuyuklugu";
            this.txtPikselBuyuklugu.Size = new System.Drawing.Size(56, 22);
            this.txtPikselBuyuklugu.TabIndex = 22;
            this.txtPikselBuyuklugu.Text = "7.2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(544, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(123, 17);
            this.label12.TabIndex = 23;
            this.label12.Text = "Piksel Büyüklüğü: ";
            // 
            // txtFazHizi
            // 
            this.txtFazHizi.Location = new System.Drawing.Point(412, 122);
            this.txtFazHizi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFazHizi.Name = "txtFazHizi";
            this.txtFazHizi.Size = new System.Drawing.Size(56, 22);
            this.txtFazHizi.TabIndex = 20;
            this.txtFazHizi.Text = "500";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(332, 126);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 17);
            this.label11.TabIndex = 21;
            this.label11.Text = "Faz Hızı: 1/";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(471, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 17);
            this.label9.TabIndex = 19;
            this.label9.Text = "mm";
            // 
            // txtAsalUzaklik
            // 
            this.txtAsalUzaklik.Location = new System.Drawing.Point(412, 95);
            this.txtAsalUzaklik.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAsalUzaklik.Name = "txtAsalUzaklik";
            this.txtAsalUzaklik.Size = new System.Drawing.Size(56, 22);
            this.txtAsalUzaklik.TabIndex = 17;
            this.txtAsalUzaklik.Text = "100";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(313, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Asal Uzaklık: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(475, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "km/h";
            // 
            // txtUcaginHizi
            // 
            this.txtUcaginHizi.Location = new System.Drawing.Point(412, 68);
            this.txtUcaginHizi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtUcaginHizi.Name = "txtUcaginHizi";
            this.txtUcaginHizi.Size = new System.Drawing.Size(56, 22);
            this.txtUcaginHizi.TabIndex = 14;
            this.txtUcaginHizi.Text = "200";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(317, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "Uçağın Hızı: ";
            // 
            // txtQ
            // 
            this.txtQ.Location = new System.Drawing.Point(877, 70);
            this.txtQ.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtQ.Name = "txtQ";
            this.txtQ.Size = new System.Drawing.Size(36, 22);
            this.txtQ.TabIndex = 12;
            this.txtQ.Text = "20";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(824, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "q (%) :";
            // 
            // txtP
            // 
            this.txtP.Location = new System.Drawing.Point(877, 42);
            this.txtP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtP.Name = "txtP";
            this.txtP.Size = new System.Drawing.Size(36, 22);
            this.txtP.TabIndex = 10;
            this.txtP.Text = "60";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(824, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "p (%) :";
            // 
            // txtPikselSayisiY
            // 
            this.txtPikselSayisiY.Location = new System.Drawing.Point(724, 42);
            this.txtPikselSayisiY.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPikselSayisiY.Name = "txtPikselSayisiY";
            this.txtPikselSayisiY.Size = new System.Drawing.Size(56, 22);
            this.txtPikselSayisiY.TabIndex = 9;
            this.txtPikselSayisiY.Text = "9420";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(563, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Piksel Sayıları: ";
            // 
            // cmbUcusYonu
            // 
            this.cmbUcusYonu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUcusYonu.FormattingEnabled = true;
            this.cmbUcusYonu.Items.AddRange(new object[] {
            "Doğu-Batı",
            "Batı-Doğu",
            "Kuzey-Güney",
            "Güney-Kuzey"});
            this.cmbUcusYonu.Location = new System.Drawing.Point(412, 38);
            this.cmbUcusYonu.Margin = new System.Windows.Forms.Padding(4);
            this.cmbUcusYonu.Name = "cmbUcusYonu";
            this.cmbUcusYonu.Size = new System.Drawing.Size(120, 24);
            this.cmbUcusYonu.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(313, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Uçuş Yönü:";
            // 
            // cmbPaftaOlcek
            // 
            this.cmbPaftaOlcek.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPaftaOlcek.FormattingEnabled = true;
            this.cmbPaftaOlcek.Items.AddRange(new object[] {
            "1/1.000",
            "1/5.000",
            "1/10.000",
            "1/25.000",
            "1/50.000",
            "1/100.000",
            "1/250.000"});
            this.cmbPaftaOlcek.Location = new System.Drawing.Point(412, 9);
            this.cmbPaftaOlcek.Margin = new System.Windows.Forms.Padding(4);
            this.cmbPaftaOlcek.Name = "cmbPaftaOlcek";
            this.cmbPaftaOlcek.Size = new System.Drawing.Size(120, 24);
            this.cmbPaftaOlcek.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::Fotogrametri.UcusPlanlama.Properties.Resources.omega_fi_kappa;
            this.pictureBox1.Location = new System.Drawing.Point(11, 10);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(201, 140);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(313, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Pafta Ölçek: ";
            // 
            // btnSonucEkrani
            // 
            this.btnSonucEkrani.BackColor = System.Drawing.Color.Maroon;
            this.btnSonucEkrani.Location = new System.Drawing.Point(6, 166);
            this.btnSonucEkrani.Margin = new System.Windows.Forms.Padding(4);
            this.btnSonucEkrani.Name = "btnSonucEkrani";
            this.btnSonucEkrani.Size = new System.Drawing.Size(20, 18);
            this.btnSonucEkrani.TabIndex = 5;
            this.btnSonucEkrani.Text = ".";
            this.btnSonucEkrani.UseVisualStyleBackColor = false;
            this.btnSonucEkrani.Click += new System.EventHandler(this.btnSonucEkrani_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(723, 100);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(26, 17);
            this.label13.TabIndex = 44;
            this.label13.Text = "cm";
            // 
            // txtFotografFormati
            // 
            this.txtFotografFormati.Location = new System.Drawing.Point(663, 97);
            this.txtFotografFormati.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFotografFormati.Name = "txtFotografFormati";
            this.txtFotografFormati.Size = new System.Drawing.Size(56, 22);
            this.txtFotografFormati.TabIndex = 42;
            this.txtFotografFormati.Text = "23";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(568, 100);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 17);
            this.label14.TabIndex = 43;
            this.label14.Text = "Foto. Formatı:";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(5, 5);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtResults);
            this.splitContainer1.Panel1.Controls.Add(this.btnSonucEkrani);
            this.splitContainer1.Panel1.Controls.Add(this.pbDraw);
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(1262, 544);
            this.splitContainer1.SplitterDistance = 966;
            this.splitContainer1.SplitterWidth = 10;
            this.splitContainer1.TabIndex = 6;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1272, 610);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.trackBar);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmMain";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "Fotogrametri - Uçuş Planlama Uygulaması";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyUp);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pbDraw)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbDraw;
        private System.Windows.Forms.TextBox txtResults;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnFlyPlan;
        private System.Windows.Forms.LinkLabel llResetPoligon;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TrackBar trackBar;
        private System.Windows.Forms.TextBox txtPikselSayisiX;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cmbUcusYonu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtQ;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPikselSayisiY;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtUcaginHizi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAsalUzaklik;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtFazHizi;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPikselBuyuklugu;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtKappa;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtFi;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtOmega;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnSonucEkrani;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.ComboBox cmbPaftaOlcek;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtYukseklik;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtKGKenari;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtDBKenari;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cmbKamera;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtFotografFormati;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}

