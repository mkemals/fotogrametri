﻿using Fotogrametri.UcusPlanlama.Repository;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

namespace Fotogrametri.UcusPlanlama
{
    public partial class frmMain : Form
    {
        protected Model.UcusParametreleri UcusParametreleri { get; set; }
        private List<Point> PolygonPoints { get; set; }
        private List<FeatRectangle> CoverRectangle { get; set; }

        #region " Graphic Nesneleri "
        private Graphics GraphicPolygon { get; set; }
        private Graphics GraphicPolygonPoints { get; set; }
        private Graphics GraphicPolygonBounds { get; set; }
        private Graphics GraphicCoverRectangle { get; set; }
        private Graphics GraphicFlyRoute { get; set; }
        private Bitmap bmp { get; set; }
        #endregion

        public frmMain()
        {
            InitializeComponent();
            CtrlKeyDown = false;
            pbDraw.Image = null;
            pbDraw.Invalidate();
            this.PolygonPoints = new List<Point>();
            // Test değerleri
            this.PolygonPoints.Add(new Point(100, 100));
            this.PolygonPoints.Add(new Point(700, 100));
            this.PolygonPoints.Add(new Point(700, 400));
            this.PolygonPoints.Add(new Point(100, 400));
            // --------------
            this.CoverRectangle = new List<FeatRectangle>();
            //
            UcusParametreleri = new Model.UcusParametreleri();
            propertyGrid1.SelectedObject = UcusParametreleri;
            cmbKamera.DataSource = new KameraModelleri().Liste;
            //
            Reset();
        }

        private void ParametreleriAl()
        {
            this.UcusParametreleri = new Model.UcusParametreleri();
            this.UcusParametreleri.UcusYonu = cmbUcusYonu.Text;
            this.UcusParametreleri.V.Val = Math.Round(double.Parse(txtUcaginHizi.Text) / 3.6, 2);
            this.UcusParametreleri.Omega.Val = double.Parse(txtOmega.Text);
            this.UcusParametreleri.Phi.Val = double.Parse(txtFi.Text);
            this.UcusParametreleri.Kappa.Val = double.Parse(txtKappa.Text);
            this.UcusParametreleri.p.Val = double.Parse(txtP.Text);
            this.UcusParametreleri.q.Val = double.Parse(txtQ.Text);
            this.UcusParametreleri.c.Val = int.Parse(txtAsalUzaklik.Text);
            this.UcusParametreleri.dt.Val = 1.0 / int.Parse(txtFazHizi.Text);
            this.UcusParametreleri.Ps.Val = double.Parse(txtPikselBuyuklugu.Text);
            this.UcusParametreleri.PikselSayilariX.Val = int.Parse(txtPikselSayisiX.Text);
            this.UcusParametreleri.PikselSayilariY.Val = int.Parse(txtPikselSayisiY.Text);
            this.UcusParametreleri.PaftaOlcek.Val = int.Parse(cmbPaftaOlcek.Text.Replace("1/", "").Replace(".", ""));
            this.UcusParametreleri.h.Val = int.Parse(txtYukseklik.Text);
            this.UcusParametreleri.la.Val = double.Parse(txtKGKenari.Text);
            this.UcusParametreleri.lb.Val = double.Parse(txtDBKenari.Text);
            this.UcusParametreleri.s.Val = int.Parse(txtFotografFormati.Text);
            this.UcusParametreleri.Kamera = (Model.Kamera)cmbKamera.SelectedItem;
            this.UcusParametreleri.Hesapla();
            propertyGrid1.SelectedObject = this.UcusParametreleri;
        }

        private void btnFlyPlan_Click(object sender, EventArgs e)
        {
            ParametreleriAl();
            DrawBounds();
            StringBuilder sbPoints = new StringBuilder();
            int pNum = 0;
            this.PolygonPoints.ForEach(f => sbPoints.Append($"    {(++pNum).ToString().PadLeft(2, ' ')}.Nokta: ({f.X}, {f.Y}) \r\n"));
            txtResults.Text += "Poligon Noktaları:\r\n" + sbPoints.ToString();
            txtResults.Text += "Poligon Alanı = " + Utilities.PolygonArea(this.PolygonPoints).ToString() + "\r\n";
        }

        private void DrawBounds()
        {
            if (this.PolygonPoints.Count < 3) // poligon için en az 3 nokta gerekli
            {
                pbDraw.Image = null;
                pbDraw.Invalidate();
                return;
            }
            Reset();
            GraphicsPath gp = new GraphicsPath();
            gp.AddLines(this.PolygonPoints.ToArray());
            gp.CloseAllFigures();
            //GraphicPolygon
            this.GraphicPolygon.DrawPath(new Pen(new SolidBrush(Color.Black)), gp);
            //GraphicPolygonPoints
            this.PolygonPoints.ForEach(f => DrawPoint(f));
            Rectangle bound = Rectangle.Truncate(gp.GetBounds());
            //GraphicPolygonBounds
            this.GraphicPolygonBounds.DrawRectangle(new Pen(new SolidBrush(Color.Red), 1), bound);
            //GraphicCoverRectangle
            this.CoverRectangle = Utilities.CoverRectIntoBound(bound, 50, 30);
            //// Tüm dikdörtgenler test için çiziliyor. Sonra sadece poligonu kaplayanlar çizilecek
            //this.CoverRectangle.ForEach(f => this.GraphicCoverRectangle.DrawRectangle(new Pen(new SolidBrush(Color.Yellow), 1), f.Rectangle));
            // Poligonu kaplayan dikdörtgenler çiziliyor
            foreach (FeatRectangle cover in this.CoverRectangle)
            {
                if (Utilities.RectanglePolygonCollision(this.PolygonPoints, cover.Rectangle))
                {
                    cover.IsCover = true;
                    this.GraphicCoverRectangle.DrawRectangle(new Pen(new SolidBrush(Color.Green), 1), cover.Rectangle);
                }
            }
            pbDraw.Image = bmp;
        }

        bool CtrlKeyDown = false;
        private void pbDraw_MouseDown(object sender, MouseEventArgs e)
        {
            if (CtrlKeyDown)
            {
                Point p = new Point(e.X, e.Y);
                DrawPoint(p);
                PolygonPoints.Add(p);
            }
            CtrlKeyDown = false;
        }

        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            CtrlKeyDown = e.Control;
        }
        private void frmMain_KeyUp(object sender, KeyEventArgs e)
        {
            CtrlKeyDown = false;
        }

        private void llResetPoligon_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CtrlKeyDown = false;
            pbDraw.Image = null;
            pbDraw.Invalidate();
            this.PolygonPoints = new List<Point>();
            this.CoverRectangle = new List<FeatRectangle>();
            Reset();
        }

        private void Reset()
        {
            this.bmp = new Bitmap(pbDraw.Width, pbDraw.Height);
            this.GraphicPolygon = Graphics.FromImage(bmp);
            this.GraphicPolygonPoints = Graphics.FromImage(bmp);
            this.GraphicPolygonBounds = Graphics.FromImage(bmp);
            this.GraphicCoverRectangle = Graphics.FromImage(bmp);
            this.GraphicFlyRoute = Graphics.FromImage(bmp);
            txtResults.Text = "";
            cmbPaftaOlcek.SelectedIndex = 0;
            cmbUcusYonu.SelectedIndex = 0;
            cmbKamera.SelectedIndex = 0;
        }

        #region " Drawing Helper "
        public void DrawPoint(Point p)
        {
            Point pB = new Point(p.X - 3, p.Y - 3);
            Point pK = new Point(p.X - 2, p.Y - 2);
            this.GraphicPolygonPoints.FillRectangle(new SolidBrush(Color.Green), new Rectangle(pB, new Size(6, 6)));
            this.GraphicPolygonPoints.FillRectangle(new SolidBrush(this.BackColor), new Rectangle(pK, new Size(4, 4)));
            pbDraw.Image = bmp;
        }

        #endregion

        private void btnSonucEkrani_Click(object sender, EventArgs e)
        {
            txtResults.Visible = !txtResults.Visible;
        }

        private void trackBar_ValueChanged(object sender, EventArgs e)
        {
            this.GraphicCoverRectangle.RotateTransform(trackBar.Value);
            pbDraw.Image = bmp;
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            this.bmp = new Bitmap(pbDraw.Width, pbDraw.Height);
        }

    }
}
