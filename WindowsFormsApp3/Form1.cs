﻿using DotSpatial.Plugins.BruTileLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BruTileLayer baseMap = BruTileLayer.CreateKnownLayer(BruTile.Predefined.KnownTileSource.OpenStreetMap, "");
            baseMap.Reproject(DotSpatial.Projections.ProjectionInfo.FromEpsgCode(4326));
            baseMap.Extent.SetCenter(new DotSpatial.Topology.Coordinate(29, 41));
            baseMap.Extent.SetValues(27, 40, 30, 42);
            map1.Layers.Add(baseMap);
        }

        bool CtrlKeyDown = false;
        private void map1_MouseDown(object sender, MouseEventArgs e)
        {

        }
    }
}
